## Repo for ecommerce fashion project nava

### Repo structure:
1. Dir **client** contains client shop. Stack: React/Redux/TS/Next
2. Dir **backend** contains backend rest api. Stack: Node/Nest/MySQL
3. Dir **admin** contains frontend admin panel (crm). Stack: React/Redux/TS
