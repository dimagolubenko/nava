import { Body, Controller, Get, Post, Req, Request, UseGuards } from "@nestjs/common";

import { CreateUserDto } from "src/users/dto/create-user.dto";
import { AuthService } from "./auth.service";
import { LocalAuthGuard } from "./local-auth.guard";
import { JwtAuthGuard } from './jwt-auth.guard';

@Controller("auth")
export class AuthController {
  constructor(
    private authService: AuthService
  ) {}

  @Post("registration")
  registration(
    @Body() userDto: CreateUserDto
  ): Promise<{token: string}> {
    return this.authService.registration(userDto);
  }

  @UseGuards(LocalAuthGuard)
  @Post("login")
  login(
    @Req() req,
  ) {
    return req.user;
  }

  @Post("logout")
  logout() {

  }

  @Get("activate/:link")
  activate() {

  }

  @Get("refresh")
  refresh() {

  }

  @UseGuards(JwtAuthGuard)
  @Get("profile")
  getProfile(@Request() req) {
    return req.user;
  }
}