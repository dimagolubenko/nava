import {BadRequestException, Injectable} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import * as bcrypt from "bcryptjs";

import { CreateUserDto } from "../users/dto/create-user.dto";
import { UserRepository } from "src/users/user.repository";
import { User } from "src/users/user.entity";

@Injectable()
export class AuthService {
  constructor(
    private userRepository: UserRepository,
    private jwtService: JwtService
  ) {}

  async registration(userDto: CreateUserDto) {
    const { email } = userDto;
    const candidate = await this.userRepository.findUserByEmail(email);
    if(candidate) {
      throw new BadRequestException(`User with email ${email} exists`);
    }
    const user = await this.userRepository.createUser(userDto);
    return this.generateToken(user);
  }

  private async generateToken(user: User) {
    const payload = { email: user.email, id: user.id };
    return {
      token: this.jwtService.sign(payload)
    }
  }

  async validateUser(email: string, password: string) {
    const user = await this.userRepository.findUserByEmail(email);
    if (user) {
      const passwordEquals = await bcrypt.compare(password, user.password);
      if (passwordEquals) {
        return this.generateToken(user);
      }
    }
  }
}