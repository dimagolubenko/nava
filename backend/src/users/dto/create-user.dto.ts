import { ApiProperty } from "@nestjs/swagger";
import {IsString, MaxLength, MinLength} from "class-validator";

export class CreateUserDto {
  @ApiProperty({
    description: "Email нового юзера",
    maxLength: 50
  })
  @IsString()
  @MaxLength(50)
  email: string;

  @ApiProperty({
    description: "Ім'я нового юзера"
  })
  @IsString()
  @MaxLength(100)
  firstName: string;

  @ApiProperty({
    description: "Прізвище нового юзера"
  })
  @IsString()
  @MaxLength(100)
  lastName: string;

  @ApiProperty({
    description: "Пароль нового юзера"
  })
  @IsString()
  @MinLength(5)
  @MaxLength(50)
  password: string;
}