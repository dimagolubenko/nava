import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  UseGuards,
  UsePipes,
  ValidationPipe
} from "@nestjs/common";
import { UserService } from "./user.service";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";

import { User } from "./user.entity";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";

@Controller("users")
export class UserController {
  constructor(private userService: UserService) { }

  @UseGuards(JwtAuthGuard)
  @Get()
  getAllUsers(): Promise<User[]> {
    return this.userService.findAllUsers();
  }

  @Get(":id")
  getUserById(@Param("id") id: number): Promise<User> {
    return this.userService.findUserById(id);
  }

  @Post()
  @UsePipes(ValidationPipe)
  @HttpCode(201)
  createUser(@Body() createUserDto: CreateUserDto): Promise<User> {
    return  this.userService.createUser(createUserDto);
  }

  @Put(":id")
  @UsePipes(ValidationPipe)
  updateUser(
    @Param("id") id: number,
    @Body() updateUserDto: UpdateUserDto
  ): Promise<User> {
    return this.userService.updateUser(id, updateUserDto);
  }

  @Delete(":id")
  @HttpCode(204)
  deleteUser(@Param("id") id: number): Promise<void> {
    return this.userService.deleteUser(id);
  }
}