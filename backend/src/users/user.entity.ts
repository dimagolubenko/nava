import { ApiProperty } from "@nestjs/swagger";
import { CoreEntity } from "src/common/entities/core.entity";
import { Column, Entity } from "typeorm";

@Entity({ name: "users" })
export class User extends CoreEntity{
  @ApiProperty({ example: 'email@email.com', description: "Email нового юзера" })
  @Column({ nullable: false })
  email: string;

  @ApiProperty({ example: 'Дмитро', description: "Ім'я нового юзера" })
  @Column({ nullable: false })
  firstName: string;

  @ApiProperty({ example: 'Голубенко', description: "Прізвище нового юзера" })
  @Column({ nullable: false })
  lastName: string;

  @ApiProperty({ example: "hfudhfwiuh53r32", description: "Пароль нового юзера" })
  @Column({ nullable: false })
  password: string;

  @ApiProperty({ description: "Профіль юзера активовао/не активовано" })
  @Column({ default: false })
  isActive: boolean;
}