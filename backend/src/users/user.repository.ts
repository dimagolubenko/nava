import { NotFoundException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import * as bcrypt from "bcryptjs";

import { User } from "./user.entity";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  finAllUsers(): Promise<User[]> {
    return this.find();
  }

  findUserById(id: number): Promise<User> {
    return this.findOne(id);
  }

  findUserByEmail(email: string): Promise<User> {
    return this.findOne({email});
  }

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    const salt = bcrypt.genSaltSync(10);
    const hashPassword = await bcrypt.hash(createUserDto.password, salt);
    const user = this.create({...createUserDto, password: hashPassword});
    return this.save(user);
  }

  async updateUser(
    id: number,
    updateUserDto: UpdateUserDto
  ): Promise<User> {
    const user = await this.findOne(id);

    if(!user) {
      throw new NotFoundException(`User with id ${id} not found`);
    }
    const { password, ...userDataWithoutPassword } = updateUserDto;

    const newUser = this.merge(
      user,
      userDataWithoutPassword
    );
    await this.save(newUser);
    return newUser;
  }

  async deleteUser(id: number): Promise<void> {
    const result = await this.delete(id);
    if(!result.affected) {
      throw new NotFoundException(`User with id ${id} not found`);
    }
  }
}