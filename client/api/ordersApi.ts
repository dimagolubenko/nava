export const ordersApi = {
  async fetchOrders(url: string) {
    const response = await fetch(url);
    return await response.json();
  }
}
