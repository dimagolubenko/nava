export const productsApi = {
  async fetchAll(url: string) {
      const response = await fetch(url);
      return await response.json();
  }
}
