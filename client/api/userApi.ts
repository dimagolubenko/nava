export const userApi = {
  async fetchUser(url: string) {
    const response = await fetch(url);
    return await response.json();
  }
}
