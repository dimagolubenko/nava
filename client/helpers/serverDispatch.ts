// Core
import { Store, Dispatch } from 'redux';

export const serverDispatch = async (store: Store, execute: (dispatch: Dispatch) => void) => {
  const { dispatch } = store;

  execute(dispatch);
}
