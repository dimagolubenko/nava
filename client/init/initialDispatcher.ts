// Core
import { GetServerSidePropsContext} from "next";
import { Store } from 'redux';

// Actions
import { productsActions } from "@/modules/products/actions";

// Selectors
import {selectIsVerified} from "@/modules/products/selectors";

// Other
import { serverDispatch } from "@/helpers/serverDispatch";

export const initialDispatcher = async (context: GetServerSidePropsContext, store: Store) => {
  await serverDispatch(store, dispatch => {
    dispatch(productsActions.setVerified());
  });

  const state = store.getState();

  const stateUpdates = {
    products: {
      isVerified: selectIsVerified(state),
    }
  };

  return {
    store,
    stateUpdates
  };
}
