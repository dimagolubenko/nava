// Core
import { combineReducers } from 'redux'

// Reducers
import { productsReducer as products } from '@/modules/products/reducer';
import { userReducer as user } from "@/modules/users/reducer";
import { ordersState as orders} from "@/modules/account/reducer";

export const rootReducer = combineReducers({
  products,
  user,
  orders
})

export type RootState = ReturnType<typeof rootReducer>;
