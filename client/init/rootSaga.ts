// Core
import { all, call } from 'redux-saga/effects'

// Watchers
import { watchProducts } from "@/modules/products/saga/watchers";
import { watchUser } from "@/modules/users/saga/watchers";
import { watchOrders } from "@/modules/account/saga/watchers";

export function* rootSaga() {
  yield all([
    call(watchProducts),
    call(watchUser),
    call(watchOrders)
  ]);
}
