// Types
import {
  OrdersInterface,
  FETCH_ORDERS_ASYNC,
  FILL_ORDERS,
  SET_ORDERS_FETCHING_ERROR,
  ErrorHttpAction,
  FetchOrdersAsyncAction,
  FillOrdersAction,
  SetOrdersFetchingErrorAction
} from './types';

// Sync
function fillOrders(payload: OrdersInterface): FillOrdersAction {
  return {
    type: FILL_ORDERS,
    payload
  }
}

function setOrdersFetchingError(payload: ErrorHttpAction): SetOrdersFetchingErrorAction {
  return {
    type: SET_ORDERS_FETCHING_ERROR,
    error: true,
    payload
  }
}

// Async
function fetchOrdersAsync(): FetchOrdersAsyncAction {
  return {
    type: FETCH_ORDERS_ASYNC
  }
}

export const ordersActions = {
  fetchOrdersAsync,
  fillOrders,
  setOrdersFetchingError
}
