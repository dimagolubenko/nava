// Core
import React, { ReactElement } from 'react';

// Other
import styles from './Orders.module.scss';

export const Orders = (): ReactElement => {
  return (
    <div className={styles.wrapper}>Orders</div>
  )
}
