// Types
import {FETCH_ORDERS_ASYNC, FILL_ORDERS, ErrorHttpAction, OrdersActionTypes, OrdersInterface, SET_ORDERS_FETCHING_ERROR} from './types';

export type OrdersState = {
  entries: OrdersInterface,
  error: false | ErrorHttpAction;
};

const initialState: OrdersState = {
  entries: null,
  error: false
};

export const ordersState = (
  state = initialState,
  action: OrdersActionTypes
): OrdersState => {
  switch (action.type) {
    case FETCH_ORDERS_ASYNC:
      return state;
    case FILL_ORDERS:
      return {
        ...state,
        entries: action.payload
      }
    case SET_ORDERS_FETCHING_ERROR:
      return {
        ...state,
        error: action.payload,
      }
    default:
    // eslint-disable-next-line no-case-declarations,@typescript-eslint/no-unused-vars
      const x: never = action;
  }

  return state;
}
