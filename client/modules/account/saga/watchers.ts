// Core
import { all, call, takeEvery } from 'redux-saga/effects';

// Types
import { FETCH_ORDERS_ASYNC } from '../types';

// Workers
import { fetchOrders } from './workers';

function* watchFetchOrders () {
  yield takeEvery(FETCH_ORDERS_ASYNC, fetchOrders);
}

export function* watchOrders() {
  yield all([
    call(watchFetchOrders)
  ])
}
