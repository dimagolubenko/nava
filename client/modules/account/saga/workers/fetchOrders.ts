// Core
import { call, put } from "redux-saga/effects";

// Api
import { ordersApi } from "@/api/index";

// Actions
import { ordersActions } from '../../actions';

// Instruments
import {verifyEnvironment} from "@/helpers/verifyEnvironment";
import {developmentLogger} from "@/helpers/logger";
import {OrdersInterface} from "@/modules/account/types";

export function* fetchOrders() {
  const { isDevelopment, isProduction } = verifyEnvironment();

  const url = "http://localhost:3004/orders";

  try {
    if (isDevelopment) {
      developmentLogger.info(`API GET request to ${url} was started...`);
    }

    const orders: OrdersInterface = yield call(ordersApi.fetchOrders, url);

    console.log('orders: ', orders);

    yield put(ordersActions.fillOrders(orders));
  } catch (error) {
    put(ordersActions.setOrdersFetchingError((error as Error).message));
  }
}
