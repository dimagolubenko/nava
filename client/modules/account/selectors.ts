import {RootState} from "@/init/rootReducer";

export const selectOrdersEntries = (state: RootState) => state.orders.entries;
