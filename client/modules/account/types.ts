interface OrderProductInterface {
  id: number;
  title: string;
  price: number;
  newPrice?: number;
  cover: {
    src: string;
    alt?: string;
  }
}

export interface OrderInterface {
  id: number;
  date: string;
  cost: number;
  status: string;
  products: OrderProductInterface[]
}

export type OrdersInterface = OrderInterface[] | null;

export type ErrorHttpAction = string;

// Sync
export const FILL_ORDERS = "FILL_ORDERS";
export type FillOrdersAction = {
  type: typeof FILL_ORDERS;
  payload: OrdersInterface;
}

export const SET_ORDERS_FETCHING_ERROR = "SET_ORDERS_FETCHING_ERROR";
export type SetOrdersFetchingErrorAction = {
  type: typeof SET_ORDERS_FETCHING_ERROR;
  error: boolean;
  payload: ErrorHttpAction;
}

// Async
export const FETCH_ORDERS_ASYNC = "FETCH_ORDERS_ASYNC";
export type FetchOrdersAsyncAction = {
  type: typeof FETCH_ORDERS_ASYNC;
}

export type OrdersActionTypes = FillOrdersAction | SetOrdersFetchingErrorAction | FetchOrdersAsyncAction;
