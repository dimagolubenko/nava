// Core
import React, { FC, ReactElement } from "react";
import Link from "next/link";

// Components
import { LoginForm } from "./LoginForm";

// Others
import styles from './Login.module.scss';

interface LoginProps {
  onShowRegisterPopup?: () => void;
}

export const Login: FC<LoginProps> = ({ onShowRegisterPopup }:LoginProps): ReactElement => {
  const showRegisterPopup = () => {
    if(onShowRegisterPopup) {
      onShowRegisterPopup();
    }
  }

  return (
    <div className={styles.wrapper}>
      <LoginForm />
      <div className={styles.createAccount}>
        <span className={styles.createAccountText}>Нет аккаунта?</span>
        {onShowRegisterPopup ? (
          <button className={styles.createAccountLink} onClick={showRegisterPopup}>Зарегистрироваться</button>
        ) : (
          <Link href="/signup">
            <a className={styles.createAccountLink}>Зарегистрироваться</a>
          </Link>
        )}
      </div>
    </div>
  )
}
