// Core
import React, { ReactElement } from 'react';
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

// Components
import { Input } from "@/shared/UI/Input";
import {Checkbox} from "@/shared/UI/Checkbox";
import {Button} from "@/shared/UI/Button";

// Other
import styles from './LoginForm.module.scss';
import { loginFormSchema } from "./schema";

interface IFormValues {
  email: string;
  password: string;
  remember?: boolean;
}

export const LoginForm = (): ReactElement => {
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<IFormValues>({
    resolver: yupResolver(loginFormSchema)
  });

  const onSubmit = (data: any) => {
    console.log(data);
    // Вызываем dispatch(authActions.login());
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
      <div className={styles.input}>
        <Input<IFormValues>
          name="email"
          label="Email"
          errors={errors}
          register={register}
          placeholder="Введите email"
          block
        />
      </div>
      <div className={styles.input}>
        <Input<IFormValues>
          name="password"
          label="Пароль"
          errors={errors}
          register={register}
          type="password"
          placeholder="Введите пароль"
          block
        />
      </div>
      <div className={styles.box}>
        <div className={styles.checkbox}>
          <Checkbox className={styles.input} name="remember" value="Запомнить" />
        </div>
        <a href="#" className={styles.link}>Забыли пароль?</a>
      </div>
      <Button type="submit" block>Войти</Button>
    </form>
  )
}
