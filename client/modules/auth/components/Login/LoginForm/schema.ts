import * as yup from 'yup';
import { SchemaOf } from 'yup';

type LoginFormSchema = {
  email: string;
  password: string;
  remember?: boolean;
}

export const loginFormSchema: SchemaOf<LoginFormSchema> = yup.object({
  email: yup.string().required('Данное поле обязательное').min(3),
  password: yup
    .string()
    .required('Данное поле обязательное')
    .min(5, ({ min }) => `Длина пароля не меньше ${min} символов`),
  remember: yup.boolean()
});
