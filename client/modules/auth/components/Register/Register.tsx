// Core
import React, { FC, ReactElement } from "react";

// Others
import styles from "./Register.module.scss";
import { RegisterForm } from "./RegisterForm";

export const Register: FC = ():ReactElement => {
  return (
    <div className={styles.wrapper}>
      <RegisterForm/>
    </div>
  )
}
