// Core
import React, { ReactElement } from 'react';
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

// Components
import { Input } from "@/shared/UI/Input";
import { Button } from "@/shared/UI/Button";

// Other
import styles from './RegisterForm.module.scss';
import { registerFormSchema } from "./schema";

interface IFormValues {
  username: string;
  email: string;
  password: string;
}

export const RegisterForm = (): ReactElement => {
  const { register, handleSubmit, formState: { errors } } = useForm<IFormValues>({
    resolver: yupResolver(registerFormSchema)
  });

  const onSubmit = (data: any) => {
    console.log(data);
    // Вызываем dispatch(authActions.register());
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
      <div className={styles.input}>
        <Input<IFormValues>
          name="username"
          label="Имя"
          errors={errors}
          register={register}
          placeholder="Введите имя"
          block
        />
      </div>
      <div className={styles.input}>
        <Input<IFormValues>
          name="email"
          label="Email"
          errors={errors}
          register={register}
          placeholder="Введите email"
          block
        />
      </div>
      <div className={styles.input}>
        <Input<IFormValues>
          name="password"
          label="Пароль"
          errors={errors}
          register={register}
          type="password"
          placeholder="Введите пароль"
          block
        />
      </div>
      <Button className={styles.submitButton} type="submit" block>Зарегистрироваться</Button>
    </form>
  )
}
