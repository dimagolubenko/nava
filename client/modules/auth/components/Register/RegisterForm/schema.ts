// Core
import * as yup from 'yup';
import { SchemaOf } from 'yup';

type RegisterFormSchema = {
  username: string;
  email: string;
  password: string;
}

export const registerFormSchema: SchemaOf<RegisterFormSchema> = yup.object({
  username: yup.string().required('Данное поле обязательное'),
  email: yup.string().required('Данное поле обязательное').min(3),
  password: yup
    .string()
    .required('Данное поле обязательное')
    .min(5, ({ min }) => `Длина пароля не меньше ${min} символов`)
});
