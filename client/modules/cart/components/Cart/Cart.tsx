// Core
import React, { FC, ReactElement } from 'react';
import Link from 'next/link';

// Components
import {Tab, Tabs} from "@/shared/UI/Tabs/Tabs";
import {Button} from "@/shared/UI/Button";

// Other
import styles from './Cart.module.scss';

const tabs: Tab[] = [
  { index: '01', title: 'Корзина', name: 'cart' },
  { index: '02', title: 'Доставка и оплата', name: 'shipping-checkout' },
  { index: '03', title: 'Подтверждение', name: 'confirmation' }
];

export const Cart: FC = (): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <h2 className={styles.pageTitle}>Корзина</h2>
      <Tabs tabs={tabs} activeTab={'01'}>
        <div className={styles.body}>
          <div className={styles.productList}>
            <div className={styles.productItem}>
              <Link href="/catalog/1">
                <div className={styles.productBody}>
                  <img className={styles.productImage} src="/images/card-image.jpg" alt="product title"/>
                  <h3 className={styles.productTitle}>Zessi Dresses</h3>
                </div>
              </Link>
              <div className={styles.quantity}>
                <span>-</span>
                <span>3</span>
                <span>+</span>
              </div>
              <div className={styles.subTotal}>
                <span>$399</span>
              </div>
              <div>
                <button className={styles.removeProduct}></button>
              </div>
            </div>

            <div className={styles.productItem}>
              <div className={styles.productBody}>
                <img className={styles.productImage} src="/images/card-image.jpg" alt="product title"/>
                <h3 className={styles.productTitle}>Zessi Dresses</h3>
              </div>
              <div className={styles.quantity}>
                <span>-</span>
                <span>3</span>
                <span>+</span>
              </div>
              <div className={styles.subTotal}>
                <span>$399</span>
              </div>
              <div>
                <button className={styles.removeProduct}></button>
              </div>
            </div>
          </div>

          <div className={styles.total}>
            <div className={styles.totalText}>
              Всего в корзине <span className={styles.totalData}>3 товара</span> на сумму <span className={styles.totalData}>1197 грн.</span>
            </div>
            <Button>Оформить заказ</Button>
          </div>
        </div>
      </Tabs>
    </div>
  )
}
