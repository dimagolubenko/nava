// Core
import React, { FC, ReactElement } from "react";

// Others
import styles from './Item.module.scss';
import {ICartProduct} from "@/shared/interfaces/ICardProduct";

interface ItemProps {
  product: ICartProduct
}

export const Item: FC<ItemProps> = ({ product }: ItemProps): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <div>
        <img
          className={styles.image}
          src={product.image}
          alt={product.title}
        />
      </div>
      <div className={styles.content}>
        <h2 className={styles.title}>{product.title}</h2>
        <div className={styles.color}>Color: {product.color}</div>
        <div className={styles.size}>Size: {product.size}</div>
        <div className={styles.row}>
          <div className={styles.count}>
            <span className={styles.countDecrease}>-</span>
            <span>{product.count}</span>
            <span className={styles.countIncrease}>+</span>
          </div>
          <span className={styles.price}>${product.price}</span>
        </div>
      </div>
      <i className={styles.close}></i>
    </div>
  )
}
