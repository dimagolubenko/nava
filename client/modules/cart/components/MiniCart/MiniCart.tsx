// Core
import React, { FC, ReactElement } from "react";

// Components
import {Item} from "./Item";
import {Button} from "@/shared/UI/Button";

// Others
import styles from './MiniCart.module.scss';
import {ICartProduct} from "@/shared/interfaces/ICardProduct";

interface MiniCartProps {
  products: ICartProduct[];
}

export const MiniCart: FC<MiniCartProps> = ({ products }: MiniCartProps): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.list}>
        {products.map(product => <Item product={product} key={product.id} />)}
      </div>
      <div>
        <div className={styles.subtotal}>
          <span>Subtotal:</span>
          <span>$176</span>
        </div>
        <Button className={styles.viewCartBtn} block color="secondary">View cart</Button>
        <Button block>Checkout</Button>
      </div>
    </div>
  )
}
