// Types
import {
  ErrorHttpAction,
  Products,
  PRODUCTS_FETCH_ASYNC,
  PRODUCTS_FILL,
  PRODUCTS_SET_FETCHING_ERROR,
  ProductsActionTypes,
  ProductsFillAction,
  ProductsSetFetchingErrorAction,
  SET_VERIFIED,
  SetVerifiedAction
} from './types';

// Sync
function setVerified(): SetVerifiedAction {
    return {
      type: SET_VERIFIED
    }
}

function fill(payload: Products): ProductsFillAction {
  return {
    type: PRODUCTS_FILL,
    payload
  }
}

function setFetchingError(payload: ErrorHttpAction): ProductsSetFetchingErrorAction {
  return {
    type: PRODUCTS_SET_FETCHING_ERROR,
    error: true,
    payload
  }
}

// Async
function fetchAsync(): ProductsActionTypes {
  return {
    type: PRODUCTS_FETCH_ASYNC
  }
}

export const productsActions = {
  fill,
  setFetchingError,
  fetchAsync,
  setVerified
}
