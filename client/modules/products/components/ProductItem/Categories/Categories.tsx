// Core
import React, { FC, ReactElement } from "react";
import Link from "next/link";

// Others
import styles from "./Categories.module.scss";

const categories = [
  { id: 1, name: "Casual and Urban Wear", path: "/" },
  { id: 2, name: "Jackets", path: "/" },
  { id: 3, name: "Men", path: "/" },
]

export const Categories: FC = (): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <span className={styles.title}>Categories:</span>
      <ul className={styles.list}>
        {categories.map((category) => (
          <li className={styles.item} key={category.id}>
            <Link href={category.path}>
              <a className={styles.link}>{category.name},</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  )
}
