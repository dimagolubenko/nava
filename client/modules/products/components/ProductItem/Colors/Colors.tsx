// Core
import React, { FC, ReactElement } from "react";

// Others
import styles from "./Colors.module.scss";
import {IColor} from "@/modules/product/interfaces/Color";

interface ColorsTypes {
  colors: IColor[]
}

export const Colors: FC<ColorsTypes> = ({ colors }: ColorsTypes): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <span className={styles.title}>Color</span>
      <div className={styles.list}>
        {colors.map(({ id, color }) => (
          <div className={styles.item} key={id}>
            <input
              className={styles.input}
              id={color + id}
              type="radio"
              name="color"
              value={color}
            />
            <label
              className={styles.label + ' selected'}
              htmlFor={color + id}
              style={{ backgroundColor: `#${color}` }}
            >
              {/*{color}*/}
            </label>
          </div>
        ) )}
      </div>
    </div>
  )
}
