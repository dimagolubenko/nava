// Core
import React, { FC, ReactElement } from "react";

// Components
import {Breadcrumbs} from "@/shared/UI/Breadcrumbs";
import {Categories} from "./Categories";
import {Tags} from "./Tags";
import {Sizes} from "./Sizes";
import {Colors} from "./Colors";
import {Button} from "@/shared/UI/Button";
import {Slider} from "@/shared/UI/Slider";

// Others
import styles from './Product.module.scss';
import {Product} from "@/shared/interfaces/Product";



const productItem: Product = {
  id: 1,
  title: "Lightweight Puffer Jacket With a Hood",
  price: 449,
  description: "Phasellus sed volutpat orci. Fusce eget lore mauris vehicula elementum gravida nec" +
    "dui. Aenean aliquam varius ipsum, non ultricies tellus sodales eu. " +
    "Donec dignissim viverra nunc, ut aliquet magna posuere eget.",
  inStock: 5,
  sku: "14056",
  sizes: [
    { id: 1, title: "xs", inStock: 2},
    { id: 2, title: "s"},
    { id: 3, title: "m", inStock: 1},
    { id: 4, title: "l"},
    { id: 5, title: "xl", inStock: 1},
  ],
  colors: [
    { id: 1, color: "222" },
    { id: 2, color: "C93A3E" },
    { id: 3, color: "E4E4E4" },
  ],
  categories: [
    { id: 1, title: "Casual and Urban Wear", slug: "casual_and_urban_wear" },
    { id: 2, title: "Jackets", slug: "jackets" },
    { id: 3, title: "Men", slug: "men" }
  ],
  tags: [
    { id: 1, title: "biker", slug: "biker" },
    { id: 2, title: "black", slug: "black" },
    { id: 3, title: "bomber", slug: "bomber" },
    { id: 4, title: "leather", slug: "leather" }
  ],
  images: [
    { id: 1, src: "https://nava.com.ua/image/cache/catalog/IMG_3808-01-01-1134x1098.jpeg", alt: "image 1", isCover: false },
    { id: 2, src: "https://nava.com.ua/image/cache/catalog/IMG_3817-01-1134x1098.jpeg", alt: "image 2", isCover: false },
    { id: 3, src: "https://nava.com.ua/image/cache/catalog/IMG_3816-01-1134x1098.jpeg", alt: "image 3", isCover: true },
    { id: 4, src: "https://nava.com.ua/image/cache/catalog/IMG_3801-02-1134x1098.jpeg", alt: "image 4", isCover: false },
    { id: 5, src: "https://nava.com.ua/image/cache/catalog/IMG_3811-01-1134x1098.jpeg", alt: "image 5", isCover: false },
    { id: 6, src: "http://localhost:3000/images/big-card-image.png", alt: "image 6", isCover: false },
  ]
};

interface ProductItemProps {
  productId: number;
}

export const ProductItem: FC<ProductItemProps> = ({ productId }: ProductItemProps): ReactElement => {
  const breadcrumbs = [
    { path: '/', title: "Shop" },
  ];

  return (
    <div className={styles.wrapper}>
      <div className="container">
        <div className="row">
          <Slider images={productItem.images}/>

          <div className="col-sm-12 col-md-5">
            <Breadcrumbs items={breadcrumbs}/>

            <div className={styles.content}>
              <h2 className={styles.title}>{productItem.title}</h2>

              <div className={styles.price}>${productItem.price}</div>

              <p className={styles.description}>{productItem.description}</p>

              <Sizes sizes={productItem.sizes}/>

              <Colors colors={productItem.colors}/>

              <div className={styles.addToCart}>
                <Button>Add to cart</Button>
              </div>

              <div className={styles.shareAndWishList}>
                <button className={styles.addToWishList}>
                  <i className={styles.wishListIcon}></i>
                  <span className={styles.wishListTitle}>add to wishlist</span>
                </button>
                <button className={styles.share}>
                  <i className={styles.shareIcon}></i>
                  <span className={styles.shareTitle}>share</span>
                </button>
              </div>

              <div className={styles.sku}>
                <span className={styles.skuTitle}>SKU:</span>
                <span>12565</span>
              </div>

              <Categories/>

              <Tags/>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
