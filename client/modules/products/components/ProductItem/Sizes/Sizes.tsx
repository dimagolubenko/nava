// Core
import React, { FC, ReactElement } from "react";

// Others
import styles from "./Sizes.module.scss";
import {ISize} from "@/modules/product/interfaces/Size";

interface SizesProps {
  sizes: ISize[]
}

export const Sizes: FC<SizesProps> = ({ sizes }: SizesProps): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <span className={styles.title}>Sizes</span>
      <div className={styles.list}>
        {sizes.map(({ id, title }) => (
          <div className={styles.item} key={id}>
            <input
              className={styles.input}
              id={title + id}
              type="radio"
              name="size"
              value={title}
            />
            <label className={styles.label} htmlFor={title + id}>{title}</label>
          </div>
        ))}
      </div>
      <a className={styles.sizeGuideLink} href="#" aria-label="size guide">Size guide</a>
    </div>
  )
}
