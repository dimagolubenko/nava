// Core
import React, { FC, ReactElement } from "react";
import Link from "next/link";

// Others
import styles from "./Tags.module.scss";

const tags = [
  { id: 1, name: "biker", path: "/" },
  { id: 2, name: "black", path: "/" },
  { id: 3, name: "bomber", path: "/" },
  { id: 4, name: "leather", path: "/" },
]

export const Tags: FC = (): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <span className={styles.title}>Tags</span>
      <ul className={styles.list}>
        {tags.map((tag) => (
          <li className={styles.item} key={tag.id}>
            <Link href={tag.path}>
              <a className={styles.link}>{tag.name},</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  )
}
