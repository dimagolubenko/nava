// Core
import React, { FC, ReactElement } from "react";
import {useSelector} from "react-redux";

// Components
import {ProductCard} from "@/shared/components/ProductCard";

// Others
import styles from './ProductList.module.scss';
import {Product} from "@/shared/interfaces/Product";
import {RootState} from "@/init/rootReducer";

export const ProductList: FC = (): ReactElement => {
  const { entries } = useSelector((state: RootState) => state.products);

  return (
    <div className={styles.wrapper}>
      {entries && entries.map(product => (
        <div className={styles.col4} key={product.id}>
          <ProductCard data={product} key={product.id}/>
        </div>
      ))}
    </div>
  )
}
