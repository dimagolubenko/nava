import styles from './FiltersButton.module.scss'

export const FiltersButton = () => {
  return (
    <button className={styles.wrapper}>Фильтры</button>
  )
}
