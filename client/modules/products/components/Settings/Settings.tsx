// Components
import {Sorting} from "./Sorting";
import {View} from "./View";
import {FiltersButton} from "./FiltersButton";

// Other
import styles from './Settings.module.scss'

export const Settings = () => {
  return (
    <div className={styles.wrapper}>
      <Sorting/>
      <View/>
      <FiltersButton/>
    </div>
  )
}
