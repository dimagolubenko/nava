// Components
import {Select} from "@/shared/UI";

// Styles
import styles from './Sorting.module.scss'

export const Sorting = () => {
  const sortingTypes = [
    { value: 'byRating', name: 'По рейтингу' },
    { value: 'byLowPriseToHight', name: 'От дешевых к дорогим' },
    { value: 'byHightPriceToLow', name: 'От дорогих к дешевым' },
    { value: 'new', name: 'Новинки' },
    { value: 'sale', name: 'Акционные' }
  ];

  return (
    <div className={styles.wrapper}>
      <Select options={sortingTypes} />
    </div>)
}
