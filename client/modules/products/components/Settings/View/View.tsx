// Core
import cx from 'classnames';

// Other
import styles from './View.module.scss';

export const View = () => {
  return (
    <div className={styles.wrapper}>
      <span>Столбцов</span>
      <ul className={styles.list}>
        <li className={styles.item}>2</li>
        <li className={styles.item}>3</li>
        <li className={cx([styles.item, styles.itemActive])}>4</li>
      </ul>
    </div>
  )
}
