// Core
import {FC, ReactElement} from "react";

// Others
import styles from './ShowMore.module.scss';

interface ShowMoreProps {
  loaded: number;
  max: number;
}

export const ShowMore: FC<ShowMoreProps> = ({ loaded, max }: ShowMoreProps): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.count}>{loaded} из {max} товаров</div>
      <div className={styles.progressLine}>
        <div className={styles.progressLineLoaded}></div>
      </div>
      <button className={styles.moreButton}>Показать ещё</button>
    </div>
  )
}
