// Core
import {FC, ReactElement, useState} from "react";
import Link from "next/link";
import cx from "classnames";

// Components
import {Dropdown} from "@/shared/UI/Dropdown";

// Others
import styles from './Categories.module.scss';

export interface Category {
  id: number;
  title: string;
  path: string;
}

interface CategoriesProps {
  data: Category[]
}

export const Categories: FC<CategoriesProps> = ({ data }: CategoriesProps): ReactElement => {
  const [selected, setSelected] = useState<number | null>(null);
  return (
    <div className={styles.wrapper}>
      <Dropdown title="Категории">
        <ul className={styles.list}>
          {data.map(({ id: categoryId, title, path }) => (
            <li className={styles.item} key={categoryId}>
              <Link href={"#"}>
                <a
                  className={cx(styles.itemLink, {[styles.itemLinkActive]: selected === categoryId})}
                  onClick={() => setSelected(categoryId)}
                >
                  {title}
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </Dropdown>
    </div>
  )
}
