// Core
import { FC, ReactElement } from 'react';
import Link from 'next/link';

// Components
import {Dropdown} from "@/shared/UI/Dropdown";

// Others
import styles from './Colors.module.scss';

export interface Color {
  id: number;
  color: string;
}

interface ColorsProps {
  data: Color[]
}

export const Colors: FC<ColorsProps> = ({ data }: ColorsProps): ReactElement => {

  return (
    <div className={styles.wrapper}>
      <Dropdown title="Цвета">
        <div className={styles.list}>
          { data.map(({ id, color }) => (
            <Link href="/" key={id}>
              <a>
                <input
                  className={styles.input}
                  type="checkbox"
                  name={color+id}
                  value={color}/>
                <label
                  className={styles.label}
                  style={{backgroundColor: "#"+color}}
                  htmlFor={color+id}></label>
              </a>
            </Link>
          )) }
        </div>
      </Dropdown>
    </div>
  )
}
