// Components
import {Categories} from "./Categories";
import {Colors} from "./Colors";
import {Sizes} from "./Sizes/Sizes";

// Others
import styles from './Sidebar.module.scss';
import {Color} from "./Colors/Colors";
import {Category} from "./Categories/Categories";
import {Size} from "./Sizes/Sizes";

// Temporary data
const categories: Category[] = [
  { id: 1, title: "Блузы", path: "/" },
  { id: 2, title: "Юбки", path: "/" },
  { id: 3, title: "Брюки", path: "/" },
  { id: 4, title: "Пиджаки", path: "/" },
]

const colors: Color[] = [
  { id: 1, color: "0A2472" },
  { id: 2, color: "D7BB4F" },
  { id: 3, color: "282828" },
  { id: 4, color: "B1D6E8" },
  { id: 5, color: "9C7539" },
  { id: 6, color: "D29B48" },
  { id: 7, color: "E6AE95" },
  { id: 8, color: "BABABA" },
  { id: 9, color: "D76B67" },
  { id: 10, color: "BFDCC4" }
];

const sizes: Size[] = [
  { id: 1, title: 'xs', path: 'xs' },
  { id: 2, title: 's', path: 's' },
  { id: 3, title: 'm', path: 'm' },
  { id: 4, title: 'l', path: 'l' },
  { id: 5, title: 'xl', path: 'xl' },
  { id: 6, title: 'xxl', path: 'xxl' },
]

export const Sidebar = () => {
  return (
    <aside className={styles.wrapper}>
      <Categories data={categories}/>
      <Colors data={colors} />
      <Sizes data={sizes}/>
    </aside>
  )
}
