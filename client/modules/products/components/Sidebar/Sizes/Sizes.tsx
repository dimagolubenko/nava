// Core
import { FC, ReactElement } from 'react';
import Link from 'next/link';

// Others
import styles from './Sizes.module.scss';
import {Dropdown} from "@/shared/UI/Dropdown";

export interface Size {
  id: number;
  title: string;
  path: string;
}

interface SizesProps {
  data: Size[]
}

export const Sizes: FC<SizesProps> = ({ data }: SizesProps):ReactElement => {

  return (
    <div className={styles.wrapper}>
      <Dropdown title="Размеры">
        <div className={styles.list}>
          {data.map(({ id: sizeId, title, path }) => (
            <Link href="#" key={sizeId}>
              <a className={styles.item}>
                <input
                  className={styles.input}
                  id={"size"+sizeId}
                  type="checkbox"
                  name="size"
                  value={title}/>
                <label
                  className={styles.label}
                  htmlFor={"size"+sizeId}
                >
                  {title}
                </label>
              </a>
            </Link>
          ))}
        </div>
      </Dropdown>
    </div>
  )
}
