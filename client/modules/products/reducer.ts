// Types
import {
  ErrorHttpAction,
  Products,
  PRODUCTS_FETCH_ASYNC,
  PRODUCTS_FILL,
  PRODUCTS_SET_FETCHING_ERROR,
  SET_VERIFIED,
  SetVerifiedAction,
  ProductsActionTypes
} from './types';

export type ProductsState = {
  entries: Products;
  error: false | ErrorHttpAction;
  isVerified: boolean;
};

const initialState: ProductsState = {
  entries: null,
  error: false,
  isVerified: false,
}

export const productsReducer = (
  state = initialState,
  action: ProductsActionTypes
): ProductsState => {
  switch (action.type) {
    case SET_VERIFIED:
      return {
        ...state,
        isVerified: true
      };
    case PRODUCTS_SET_FETCHING_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case PRODUCTS_FILL:
      return {
        ...state,
        entries: action.payload,
        error: false
      };
    case PRODUCTS_FETCH_ASYNC:
      return state;
    default:
      // eslint-disable-next-line no-case-declarations,@typescript-eslint/no-unused-vars
      const x: never = action;
  }

  return state;
}
