// Core
import { takeEvery, all, call } from "redux-saga/effects";

// Types
import { PRODUCTS_FETCH_ASYNC } from '../types';

// Workers
import { fetchProducts } from './workers/fetchProducts';

export function* watchFetchProducts() {
  yield takeEvery(PRODUCTS_FETCH_ASYNC, fetchProducts);
}

export function* watchProducts() {
  yield all([
    call(watchFetchProducts)
  ]);
}
