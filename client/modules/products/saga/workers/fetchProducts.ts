// Core
import { put, call } from 'redux-saga/effects';
import fetch from 'isomorphic-unfetch';

// Actions
import { productsActions } from "../../actions";

// Instruments
import {Products} from "../../types";
import {verifyEnvironment} from "@/helpers/verifyEnvironment";
import {developmentLogger} from "@/helpers/logger";
import { productsApi } from "@/api/productsApi";

export function* fetchProducts() {
  const { isDevelopment, isProduction } = verifyEnvironment();

  const url = "http://localhost:3004/products";

  try {
    if (isDevelopment) {
      developmentLogger.info(`API GEt request to ${url} was started...`);
    }

    const products: Products = yield call(productsApi.fetchAll, url);

    yield put(productsActions.fill(products));

  } catch (error) {
    yield put(productsActions.setFetchingError((error as Error).message));
  }
}
