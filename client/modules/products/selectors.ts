import {RootState} from "@/init/rootReducer";

export const selectProductsEntries = (state: RootState) => state.products.entries;
export const selectIsVerified = (state: RootState) => state.products.isVerified;
