// Interfaces
import {Product} from "@/shared/interfaces/Product";

export type Products = Product[] | null;

export type ErrorHttpAction = string;

// Sync
export const SET_VERIFIED = "SET_VERIFIED";
export type SetVerifiedAction = {
  type: typeof SET_VERIFIED;
}

export const PRODUCTS_FILL = "PRODUCTS_FILL";
export type ProductsFillAction = {
  type: typeof PRODUCTS_FILL;
  payload: Products;
}

export const PRODUCTS_SET_FETCHING_ERROR = "PRODUCTS_SET_FETCHING_ERROR";
export type ProductsSetFetchingErrorAction = {
  type: typeof PRODUCTS_SET_FETCHING_ERROR;
  error: boolean;
  payload: ErrorHttpAction;
}

// Async
export const PRODUCTS_FETCH_ASYNC = "PRODUCTS_FETCH_ASYNC";
type ProductsFetchAsyncAction = {
  type: typeof PRODUCTS_FETCH_ASYNC;
}

export type ProductsActionTypes = ProductsFillAction
  | ProductsSetFetchingErrorAction
  | ProductsFetchAsyncAction
  | SetVerifiedAction;
