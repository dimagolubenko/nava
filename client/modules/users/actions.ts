// Types
import {
  FETCH_USER_ASYNC,
  FILL_USER,
  FillUserAction,
  IUser,
  SET_USER_FETCHING_ERROR,
  SetUserFetchingErrorAction,
  UserActionTypes,
} from './types';
import {ErrorHttpAction} from "@/modules/products/types";

// Sync
function fillUser(payload: IUser): FillUserAction {
  return {
    type: FILL_USER,
    payload
  }
}

function setUserFetchingError(payload: ErrorHttpAction): SetUserFetchingErrorAction {
  return {
    type: SET_USER_FETCHING_ERROR,
    error: true,
    payload
  }
}

// Async
function fetchUserAsync(): UserActionTypes {
  return {
    type: FETCH_USER_ASYNC
  }
}

export const userActions = {
  fillUser,
  setUserFetchingError,
  fetchUserAsync,
}
