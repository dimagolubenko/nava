// Types
import {ErrorHttpAction, FETCH_USER_ASYNC, FILL_USER, IUser, SET_USER_FETCHING_ERROR, UserActionTypes} from './types';

export type UserState = {
  entry: IUser | null;
  error: false | ErrorHttpAction;
}

const initialState: UserState = {
  entry: null,
  error: false,
}

export const userReducer = (
  state = initialState,
  action: UserActionTypes
): UserState => {
  switch (action.type) {
    case SET_USER_FETCHING_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case FILL_USER:
      return {
        ...state,
        entry: action.payload
      };
    case FETCH_USER_ASYNC:
      return state;
    default:
      // eslint-disable-next-line no-case-declarations,@typescript-eslint/no-unused-vars
      const x: never = action;
  }

  return state;
}
