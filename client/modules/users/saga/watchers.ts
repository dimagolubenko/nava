// Core
import { takeEvery, all, call } from "redux-saga/effects";

// Types
import { FETCH_USER_ASYNC } from '../types';

// Workers
import { fetchUser } from './workers/fetchUser';

export function* watchFetchUser() {
  yield takeEvery(FETCH_USER_ASYNC, fetchUser);
}

export function* watchUser() {
  yield all([
    call(watchFetchUser)
  ])
}
