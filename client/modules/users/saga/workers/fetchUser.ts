// Core
import { put, call } from 'redux-saga/effects';

// Actions
import { userActions } from "../../actions";

// Instruments
import {IUser} from "../../types";
import {verifyEnvironment} from "@/helpers/verifyEnvironment";
import {developmentLogger} from "@/helpers/logger";
import { userApi } from "@/api/index";

export function* fetchUser() {
  const { isDevelopment, isProduction } = verifyEnvironment();
  const url = "http://localhost:3004/user";

  try {
    if (isDevelopment) {
      developmentLogger.info(`API GET request to ${url} was started...`);
    }

    const user: IUser = yield call(userApi.fetchUser, url);

    yield put(userActions.fillUser(user));

  } catch (error) {
    yield put(userActions.setUserFetchingError((error as Error).message));
  }
}
