import { RootState } from "@/init/rootReducer";

export const selectUserEntry = (state: RootState) => state.user.entry;
