export interface IUser {
  id: number;
  name: string;
  role: string;
  token: string;
};

export type ErrorHttpAction = string;

// Sync
export const FILL_USER = "FILL_USER";
export type FillUserAction = {
  type: typeof FILL_USER,
  payload: IUser | null
}

export const SET_USER_FETCHING_ERROR = "USER_SET_FETCHING_ERROR";
export type SetUserFetchingErrorAction = {
  type: typeof SET_USER_FETCHING_ERROR;
  error: boolean;
  payload: ErrorHttpAction;
}

// Async
export const FETCH_USER_ASYNC = "FETCH_USER_ASYNC";
export type FetchUserAsyncAction = {
  type: typeof FETCH_USER_ASYNC
}

export type UserActionTypes = FillUserAction
  | SetUserFetchingErrorAction
  | FetchUserAsyncAction;
