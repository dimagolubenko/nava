// module.exports = {
//   reactStrictMode: true,
// }

const withPluging = require('next-compose-plugins');

module.exports = withPluging([], {
  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.resolve.fallback.fs = false;
    }

    return config;
  }
});
