// Core
import { Provider } from 'react-redux'
import Head from 'next/head'
import { appWithTranslation } from 'next-i18next';

// Other
import '../styles/styles.scss'
import { useStore } from '../init/store'
import nextI18NextConfig from '../init/next-i18next.config';
import { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  const store = useStore(pageProps.initialReduxState);

  return (
    <>
      <Head>
        <title>Nava</title>
      </Head>
      <Provider store={store}>
        <Component theme='default' {...pageProps} />
      </Provider>
    </>
  )
}
export default appWithTranslation(MyApp, nextI18NextConfig);
