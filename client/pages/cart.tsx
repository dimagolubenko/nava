// Components
import { CommonLayout } from '@/shared/layouts/CommonLayout'
import {Cart} from "@/modules/cart/components/Cart/Cart";

export default function CartPage() {
  return (
    <>
      <CommonLayout>
        <div className="container">
          <Cart/>
        </div>
      </CommonLayout>

    </>
  )
}
