// Core
import {NextPageContext, NextPage} from "~/next";

// Components
import { ProductItem } from "@/modules/products/components/ProductItem";

// Others
import styles from './product.module.scss'
import {CommonLayout} from "@/shared/layouts/CommonLayout";

export const getServerSideProps = ({ query }: NextPageContext) => {
  const { product: productId } = query;
  return {
    props: {
      productId
    }
  }
}

interface ProductPops {
  productId: number;
}

const ProductPage: NextPage<ProductPops> = ({ productId }: ProductPops) => {
  return (
    <CommonLayout>
      <div className={styles.wrapper}>
        <ProductItem productId={productId}/>
      </div>
    </CommonLayout>
  )
}

export default ProductPage;
