// Core
import Link from "next/link";
import cx from 'classnames';
import {GetServerSidePropsContext} from "next";
import R from 'ramda';

// Components
import { CommonLayout } from '@/shared/layouts/CommonLayout'
import {Settings} from "@/modules/products/components/Settings";
import {ProductList} from "@/modules/products/components/ProductList";
import {ShowMore} from "@/modules/products/components/ShowMore";
import {Sidebar} from "@/modules/products/components/Sidebar";
import {Breadcrumbs} from "@/shared/UI/Breadcrumbs";

// Actions
import {productsActions} from "@/modules/products/actions";
import {userActions} from '@/modules/users/actions';

// Selectors
import {selectProductsEntries} from "@/modules/products/selectors";
import {selectUserEntry} from "@/modules/users/selectors";

// Others
import styles from './catalog.module.scss'
import { initializeStore } from "@/init/store";
import { initialDispatcher } from "@/init/initialDispatcher";
import { disableSaga } from "@/helpers/disableSaga";
import {serverDispatch} from "@/helpers/serverDispatch";

export const getServerSideProps = async (context: GetServerSidePropsContext) => {
  const { store, stateUpdates } = await initialDispatcher(context, initializeStore());

  await serverDispatch(store, dispatch => {
    dispatch(userActions.fetchUserAsync());
    dispatch(productsActions.fetchAsync());
  });

  await disableSaga(store);

  const state = store.getState();

  const currentPageReduxState = {
    products: {
      entries: selectProductsEntries(state),
    },
    user: {
      entry: selectUserEntry(state)
    },
  };

  const initialReduxState = R.mergeDeepRight(
    stateUpdates,
    currentPageReduxState
  );

  return {
    props: {
      initialReduxState
    }
  }
}

export default function Catalog() {
  const activeLinkStyles = cx([styles.categoriesItem, styles.active])

  return (
    <>
      <CommonLayout>
        <div className={styles.wrapper}>
          <div className="container">
            <ul className={styles.categories}>
              <li className={activeLinkStyles}>
                <Link href='/catalog'>
                  <a>Блузы</a>
                </Link>
              </li>
              <li className={styles.categoriesItem}>
                <Link href='/catalog'>
                  <a>Юбки</a>
                </Link>
              </li>
              <li className={styles.categoriesItem}>
                <Link href='/catalog'>
                  <a>Брюки</a>
                </Link>
              </li>
            </ul>

            <div className={styles.contentWrapper}>
              <div className={styles.leftRow}>
                <Sidebar/>
              </div>
              <div className={styles.rightRow}>
                <div className={styles.catalogTop}>
                  <Breadcrumbs items={[{ path: "/catalog", title: "Каталог" }]}/>
                  <Settings/>
                </div>

                <ProductList/>

                <ShowMore loaded={36} max={497}/>
              </div>
            </div>

          </div>
        </div>
      </CommonLayout>
    </>
  )
}
