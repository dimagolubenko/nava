// Core
import {GetServerSidePropsContext} from "next";

// Components
import { CommonLayout } from '@/shared/layouts/CommonLayout'

// Helpers

export const getServerSideProps = (context: GetServerSidePropsContext) => {
    return {
        redirect: {
            destination: '/catalog'
        }
    }
}

export default function Home() {
  return (
      <>
        <CommonLayout>
          Index page
        </CommonLayout>
      </>
  )
}
