// Components
import { CommonLayout } from '@/shared/layouts/CommonLayout';
import { Login } from '@/modules/auth/components/Login';

// Other
import styles from './login.module.scss';

export default function LoginPage() {
  return (
    <CommonLayout>
      <div className="container">
        <div className="row center-xs">
          <div className="col-md-6">
            <h2 className={styles.title}>Вход</h2>
            <Login />
          </div>
        </div>
      </div>
    </CommonLayout>
  )
}
