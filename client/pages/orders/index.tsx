// Core
import { ReactElement } from 'react';
import R from "ramda";
import {GetServerSidePropsContext} from "next";

// Components
import { Orders } from "@/modules/account/components/Orders";
import { CommonLayout } from '@/shared/layouts/CommonLayout'

// Actions
import { ordersActions } from "@/modules/account/actions";

// Selectors
import { selectOrdersEntries } from "@/modules/account/selectors";

// Others
import styles from './orders.module.scss';
import {initialDispatcher} from "@/init/initialDispatcher";
import {initializeStore} from "@/init/store";
import {serverDispatch} from "@/helpers/serverDispatch";
import {disableSaga} from "@/helpers/disableSaga";

export const getServerSideProps = async (context: GetServerSidePropsContext) => {
  const { store, stateUpdates } = await initialDispatcher(context, initializeStore());

  await serverDispatch(store, dispatch => {
    dispatch(ordersActions.fetchOrdersAsync());
  });

  await disableSaga(store);

  const currentPageReduxState = {
    orders: {
      entries: selectOrdersEntries(store.getState())
    }
  };

  const initialReduxState = R.mergeDeepRight(
    stateUpdates,
    currentPageReduxState
  );

  return {
    props: {
      initialReduxState
    }
  }
}

export default function OrdersPage(): ReactElement {
  return (
    <CommonLayout>
      <div className={styles.wrapper}>
        <div className="container">
          <Orders/>
        </div>
      </div>
    </CommonLayout>
  )
}
