// Components
import { CommonLayout } from '@/shared/layouts/CommonLayout';
import { Register } from '@/modules/auth/components/Register';

// Other
import styles from './register.module.scss';

export default function RegisterPage() {
  return (
    <CommonLayout>
      <div className={styles.wrapper}>
        <div className="container">
          <div className="row center-xs">
            <div className="col-md-6">
              <h2 className={styles.title}>Зарегистрироваться</h2>
              <Register />
            </div>
          </div>
        </div>
      </div>
    </CommonLayout>
  )
}
