import styles from './Breadcrumbs.module.scss'
import {FC, ReactElement} from "react";
import Link from 'next/link'

interface IBreadcrumb {
  path: string;
  title: string;
}

interface BreadcrumbsProps {
  items?: IBreadcrumb[]
}

export const Breadcrumbs: FC<BreadcrumbsProps> = ({ items }: BreadcrumbsProps): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <Link href='/'><a className={styles.item}>Главная</a></Link>
      {items && items.map(({ path, title }, index) => (
        <span key={title+index}>
          <span> / </span>
          <Link href={path}>
            <a className={styles.item}>{title}</a>
          </Link>
        </span>
      ))}
    </div>
  )
}
