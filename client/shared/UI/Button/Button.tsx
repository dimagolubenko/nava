// Core
import React, { FC, ReactElement, ReactNode } from "react";
import cx from "classnames";

// Others
import styles from './Button.module.scss';

//Types
type Variant = "contained" | "outlined";
type Size = "small" | "middle" | "large";
type Color = "primary" | "secondary" | "info" | "success" | "danger";
type Type = "button" | "submit" | "reset";

interface ButtonProps {
  children: ReactNode;
  variant?: Variant;
  size?: Size;
  color?: Color;
  type?: Type;
  block?: boolean;
  className?: string;
}

export const Button: FC<ButtonProps> = (props: ButtonProps): ReactElement => {
  const {
    children,
    variant = "contained",
    size = "middle",
    color = "primary",
    type = "button",
    block = false,
    className
  }: ButtonProps = props;

  const buttonClasses = cx(
    styles.wrapper,
    className,
    {
      [styles.contained]: variant === "contained",
      [styles.outlined]: variant === "outlined",
      [styles.primary]: color === "primary",
      [styles.secondary]: color === "secondary",
      [styles.info]: color === "info",
      [styles.success]: color === "success",
      [styles.danger]: color === "danger",
      [styles.middle]: size === "middle",
      [styles.block]: block
    }
  )

  return (
    <button className={buttonClasses} type={type}>
      <span className={styles.text}>{children}</span>
    </button>
  )
}
