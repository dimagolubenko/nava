import React, { FC, ReactElement } from 'react';
import cx from 'classnames';
import styles from './Checkbox.module.scss';

interface CheckboxProps {
  name: string;
  value: string;
  className?: string;
  isChecked?: boolean;
}

export const Checkbox: FC<CheckboxProps> = ({ name, value, className, isChecked }: CheckboxProps):ReactElement  => {
  return (
    <>
      <input
        className={cx(styles.input, className && className)}
        id={name}
        type="checkbox"
        name={name}
        checked={isChecked}
      />
      <label className={styles.label} htmlFor={name}>{value}</label>
    </>
  )
}
