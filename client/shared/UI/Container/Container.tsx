// Core
import React, { FC, ReactNode, ReactElement } from 'react'
import styles from './Container.module.scss'

type PropTypes = {
  children: ReactNode
}

export const Container: FC<PropTypes> = ({ children }: PropTypes): ReactElement => {
  return <div className={styles.container}>{children}</div>
}