// Core
import {ReactNode, FC, ReactElement, useState} from "react";
import cx from 'classnames';

// Others
import styles from './Dropdown.module.scss';

interface DropdownProps {
  children?: ReactNode;
  title: string;
}

export const Dropdown:FC<DropdownProps> = ({ children, title }: DropdownProps):ReactElement => {
  const [isOpened, setIsOpened] = useState(true)
  return (
    <div className={styles.wrapper}>
      <h2 className={styles.title} onClick={() => setIsOpened(!isOpened)}>
        <span>{title}</span>
        <i className={cx(styles.expand, isOpened ? [styles.opened] : [styles.closed])}></i>
      </h2>
      <div className={cx(styles.body, !isOpened && [styles.bodyHidden])}>
        {children}
      </div>
    </div>
  )
}
