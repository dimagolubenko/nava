// Core
import React, { ReactElement } from "react";
import cx from 'classnames';
import { UseFormRegister, Path, FieldErrors } from "react-hook-form";

// Others
import styles from './Input.module.scss';

interface InputProps<IFormValues> {
  name: Path<IFormValues>;
  register: UseFormRegister<IFormValues>;
  errors: FieldErrors;
  className?: string;
  type?: string;
  placeholder?: string;
  block?: boolean;
  label?: string;
}

export const Input = function <IFormValues>({
  type = "text",
  className,
  name,
  label,
  placeholder,
  block,
  register,
  errors
}: InputProps<IFormValues>):ReactElement {
  const inputClasses = cx(styles.input, className, { [styles.block]: block });

  return (
    <>
      <input
        className={inputClasses}
        {...register(name)}
        id={name + "-input"}
        type={type}
        placeholder={placeholder}
      />
      <div className={styles.errorMessage}>{errors[name]?.message}</div>
      {label && <label className={styles.label} htmlFor={name + "-input"}>{label}</label>}
    </>
  )
}
