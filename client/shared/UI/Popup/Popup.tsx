// Core
import { FC, ReactNode, ReactElement, useRef, MouseEvent } from 'react';
import cx from 'classnames';

// Others
import styles from './Popup.module.scss';
import {PopupPosition} from "@/shared/interfaces/PopupPosition";

interface PopupProps {
  position: PopupPosition;
  children?: ReactNode;
  title?: string;
  close?: boolean;
  isOpened: boolean;
  onChangeVisible: (visibleStatus: boolean) => void;
}


export const Popup: FC<PopupProps> = (props:PopupProps): ReactElement | null => {
  const {
    children,
    position,
    title,
    close = true,
    isOpened,
    onChangeVisible
  } = props;

  const wrapperRef = useRef<HTMLDivElement>(null)

  const popupClasses = cx(
    styles.popup,
    {
      [styles.center]: position === PopupPosition.Center,
      [styles.right]: position === PopupPosition.Right,
      [styles.fullscreen]: position === PopupPosition.FullScreen
    });

  const handleWrapperClick = (e: MouseEvent<HTMLDivElement>) => {
    if(e.target === wrapperRef.current) {
      onChangeVisible(false);
    }
  }

  if (!isOpened) {
    return null;
  }

  return (
    <div className={styles.wrapper}
         ref={wrapperRef}
         onClick={handleWrapperClick}>
      <div className={popupClasses}>
      {title && (
        <header className={styles.header}>
          <span className={styles.title}>{title}</span>
          {close && <i className={styles.close} onClick={() => onChangeVisible(false)}></i>}
        </header>
      )}
        <div className={styles.body}>{children}</div>
      </div>
    </div>
  )
}
