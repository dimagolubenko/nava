// Core
import React from "react"
import cx from 'classnames'

// Styles
import styles from './Select.module.scss'

export type OptionType = {
  value: string;
  name: string;
}

type SelectProps = {
  defaultValue?: string;
  children?: React.ReactNode;
  options: OptionType[],
  className?: string;
}

export const Select = ({ options, defaultValue, className }: SelectProps) => {
  const selectClasses = cx(`${styles.select}`, { [`${className}`]: className })

  return (
    <select className={selectClasses}>
      {defaultValue && <option value="" disabled selected>{defaultValue}</option>}
      {options.map(({ value, name }) => (
        <option value={value} key={value}>{name}</option>
      ))}
    </select>
  )
}