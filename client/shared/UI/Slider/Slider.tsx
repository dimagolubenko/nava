// Core
import React, { FC, ReactElement, useState } from 'react';
import cx from 'classnames';

// Others
import styles from './Slider.module.scss';

interface IImage {
  id: number;
  src: string;
  alt?: string;
  isCover?: boolean;
}

interface SliderProps {
  images: IImage[]
}

export const Slider: FC<SliderProps> = ({ images }: SliderProps): ReactElement => {
  const [bigImage, setBigImage] = useState<IImage | void>(images.find(image => image.isCover === true));

  const changeBigImage = (imageId: number) => {
    setBigImage(images.find(image => image.id === imageId));
  }

  return (
    <>
      <div className="col-xs-2 col-md-1">
        <div className={styles.miniatures}>
          {images.map((image) => (
            <div
              className={styles.miniatureItem}
              key={image.id}
            >
              <img
                className={cx(
                  styles.miniatureImage,
                  { [styles.selected]: bigImage && image.id === bigImage.id}
                )}
                src={image.src}
                alt={image.alt}
                onClick={() => changeBigImage(image.id)}
              />
            </div>
          ))}
        </div>
      </div>

      {bigImage && (
        <div className="col-xs-10 col-md-6">
          <div className={styles.bigImageWrapper}>
            <img className={styles.bigImage} src={bigImage.src} alt={bigImage.alt}/>
          </div>
        </div>
      )}
    </>
  )
}
