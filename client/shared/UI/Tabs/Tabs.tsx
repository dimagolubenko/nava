// Core
import React, { FC, ReactElement, ReactChild } from 'react';
import cx from 'classnames';

// Other
import styles from './Tabs.module.scss';

export type Tab = {
  index: string;
  title: string;
  name: string;
}

type TabsProps = {
  tabs: Tab[];
  children: ReactChild;
  activeTab: string;
}

export const Tabs: FC<TabsProps> = ({ tabs, children, activeTab }: TabsProps): ReactElement => {
  const tabClasses = (tabIndex: string) => tabIndex === activeTab ? cx(styles.tabItem, styles.active) : styles.tabItem;
  return (
    <div className={styles.wrapper}>
      <div className={styles.tabList}>
        { tabs.map((tab) => (
          <div className={tabClasses(tab.index)} key={tab.name}>{tab.index}&nbsp;&nbsp;{tab.title}</div>
        )) }
      </div>
      <div className={styles.body}>{children}</div>
    </div>
  )
}
