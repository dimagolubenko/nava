// Core
import Image from 'next/image'
import Link from 'next/link'
import cx from 'classnames'
import React, {useState} from "react";
import {useSelector} from "react-redux";

// Components
import {Container, OptionType, Select} from '@/shared/UI'
import {Popup} from "@/shared/UI/Popup";
import {Login} from "@/modules/auth/components/Login";
import {Register} from "@/modules/auth/components/Register";
import {MiniCart} from "@/modules/cart/components/MiniCart";

// Selectors
import {selectUserEntry} from "@/modules/users/selectors";

// Instruments
import styles from './CommonHeader.module.scss'
import logo from '../../../public/images/logo.jpg'
import {PopupPosition} from "@/shared/interfaces/PopupPosition";
import {CartProduct} from "@/shared/interfaces/CardProduct";

const languages: OptionType[] = [
  { name: "Українська", value: "ua" },
  { name: "Русский", value: "ru" },
  { name: "English", value: "en" },
]

const currencies: OptionType[] = [
  { name: "грн. UA", value: "ua" },
  { name: "руб. RU", value: "ru" },
  { name: "$ USD", value: "en" },
]

const cartProducts: CartProduct[] = [
  {
    id: 1,
    title: "Zessi Dresses",
    color: "Yellow",
    size: "L",
    image: "/images/card-image.jpg",
    count: 4,
    price: 99
  },
  {
    id: 2,
    title: "Kirby T-Shirt",
    color: "Yellow",
    size: "L",
    image: "/images/card-image.jpg",
    count: 3,
    price: 99
  },
  {
    id: 3,
    title: "Cableknit Shawl",
    color: "Yellow",
    size: "L",
    image: "/images/card-image.jpg",
    count: 4,
    price: 99
  },
  {
    id: 4,
    title: "Cableknit Shawl",
    color: "Yellow",
    size: "L",
    image: "/images/card-image.jpg",
    count: 4,
    price: 99
  },
  {
    id: 5,
    title: "Cableknit Shawl",
    color: "Yellow",
    size: "L",
    image: "/images/card-image.jpg",
    count: 4,
    price: 99
  },
]

export const CommonHeader = () => {
  const [loginIsOpened, setLoginIsOpened] = useState(false);
  const [registerIsOpened, setRegisterIsOpened] = useState(false);
  const [cartWidgetIsOpened, setCartWidgetIsOpened] = useState(false);
  const user = useSelector(selectUserEntry)

  const handleChangeLoginVisible = (visibleStatus: boolean) => {
    setLoginIsOpened(visibleStatus);
  }
  const handleChangeRegisterVisible = (visibleStatus: boolean) => {
    setRegisterIsOpened(visibleStatus);
  }

  const handleChangeCartWidgetVisible = (visibleStatus: boolean) => {
    setCartWidgetIsOpened(visibleStatus);
  }

  const handleShowRegisterPopup = () => {
    setLoginIsOpened(false);
    setRegisterIsOpened(true);
  }

  return (
    <header className={styles.header}>
      <Container>
        <div className={styles.headerInner}>
          <Link href="/">
            <div className={styles.logo}>
              <Image src={logo} alt="NAVA" layout="responsive" className={styles.logo}/>
            </div>
          </Link>
          <nav className={styles.menu}>
            <Link href="/catalog">
              <a className={cx(`${styles.menuLink}`, `${styles.menuLinkActive}`)}>Каталог</a>
            </Link>
            <Link href="/">
              <a className={styles.menuLink}>Акции</a>
            </Link>
          </nav>
          <Select className={styles.language} options={languages}/>
          <Select className={styles.currency} options={currencies}/>
          <button className={styles.search}></button>
          {user ? (
            <Link href="/orders">
              <a className={styles.ordersLink}></a>
            </Link>
          ) : (
            <button className={styles.user} onClick={() => setLoginIsOpened(true)}></button>
          )}
          <button className={styles.favorite}></button>
          <button className={styles.cart} onClick={() => setCartWidgetIsOpened(true)}>
            <span className={styles.cartCount}>{cartProducts.length}</span>
          </button>
          <button className={styles.burger}></button>
        </div>
      </Container>
      <Popup position={PopupPosition.Right}
             title="Login"
             isOpened={loginIsOpened}
             onChangeVisible={handleChangeLoginVisible}
      >
        <Login onShowRegisterPopup={handleShowRegisterPopup}/>
      </Popup>
      <Popup position={PopupPosition.Right}
             title="Create an account"
             isOpened={registerIsOpened}
             onChangeVisible={handleChangeRegisterVisible}
      >
        <Register/>
      </Popup>
      <Popup position={PopupPosition.Right}
             title={`Shopping bag ( ${cartProducts.length} )`}
             isOpened={cartWidgetIsOpened}
             onChangeVisible={handleChangeCartWidgetVisible}>
        <MiniCart products={cartProducts}/>
      </Popup>
    </header>
  )
}
