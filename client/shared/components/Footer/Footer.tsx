// Core
import styles from './Footer.module.scss'
import cx from 'classnames';

// Components
import { Container } from '@/shared/UI'
import {FooterMenu} from "@/shared/components/FooterMenu";

// Others
import {FooterMenuLink} from "@/shared/components/FooterMenu/FooterMenu";

const companyMenu: FooterMenuLink[] = [
  { linkTitle: "About us", path: "/" },
  { linkTitle: "Careers", path: "/" },
  { linkTitle: "Affilates", path: "/" },
  { linkTitle: "Blog", path: "/" },
  { linkTitle: "Contact Us", path: "/" }
];

const shopMenu: FooterMenuLink[] = [
  { linkTitle: "New Arrivals", path: "/" },
  { linkTitle: "Accessories", path: "/" },
  { linkTitle: "Men", path: "/" },
  { linkTitle: "Women", path: "/" },
  { linkTitle: "Shop All", path: "/" }
];

const helpMenu: FooterMenuLink[] = [
  { linkTitle: "Customer Service", path: "/" },
  { linkTitle: "My Account", path: "/" },
  { linkTitle: "Find a Store", path: "/" },
  { linkTitle: "Legal & Privacy", path: "/" },
  { linkTitle: "Contact", path: "/" },
  { linkTitle: "Gift Card", path: "/" }
];

export const Footer = () => {
  return (
    <footer className={styles.wrapper}>
      <Container>
        <div className={styles.inner}>
          <div className={styles.leftRow}>
            <div>
              <img className={styles.logo} src="/images/logo.jpg" alt="NAVA"/>
            </div>
            <div className={styles.contacts}>
              <p>Украина, Киев</p>
              <a href="mailto:hello@nava.com">hello@nava.com</a>
              <p>+1 246-345-0695</p>
            </div>
            <div className={styles.social}>
              <a href="/" target="_blank">
                <i className={cx(styles.socialIcon, styles.facebook)}></i>
              </a>
              <a href="/" target="_blank">
                <i className={cx(styles.socialIcon, styles.instagram)}></i>
              </a>
            </div>
          </div>

          <FooterMenu menuTitle="Company" list={companyMenu}/>
          <FooterMenu menuTitle="Shop" list={shopMenu}/>
          <FooterMenu menuTitle="Help" list={helpMenu}/>
        </div>
      </Container>
    </footer>
  )
}
