// Core
import {FC, ReactElement} from "react";
import Link from "next/link";

// Others
import styles from "./FooterMenu.module.scss";

export interface FooterMenuLink {
  linkTitle: string;
  path: string;
}

interface FooterMenuProps {
  menuTitle: string;
  list: FooterMenuLink[]
}

export const FooterMenu: FC<FooterMenuProps> = ({ menuTitle, list }: FooterMenuProps): ReactElement => {
  return (
    <div className={styles.wrapper}>
      <h2 className={styles.title}>{menuTitle}</h2>
      <ul className={styles.list}>
        {list.map(({ linkTitle, path }, index) => (
          <li className={styles.item} key={linkTitle + index}>
            <Link href={path}>
              <a>{linkTitle}</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  )
}
