// Core
import {useState} from "react";
import cx from "classnames";

// Others
import styles from './Colors.module.scss'

const colors = [
  { id: 1, title: "Черный", color: "#222" },
  { id: 2, title: "Коричневый", color: "#B9A16B" },
  { id: 3, title: "Персиковый", color: "#F5E6E0" }
]

export const Colors = () => {
  const [activeColor, setActiveColor] = useState(colors[0].id)

  const changeActive = (id) => {
    setActiveColor(id)
  }

  return (
    <div className={styles.wrapper}>
      {colors.map(({ id, color }) => (
        <i
          className={cx({[styles.itemActive]: activeColor === id }, styles.item)}
          style={{backgroundColor: color}}
          key={id}
          onClick={() => changeActive(id)}
        >
        </i>
      ))}
    </div>
  )
}
