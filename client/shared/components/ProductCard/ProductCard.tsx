// Core
import {FC, ReactElement} from "react";
import Link from 'next/link';
import cx from "classnames";

// Components
import {Colors} from "./Colors";

// Others
import styles from './ProductCard.module.scss'
import {Product} from "@/shared/interfaces/Product";

interface ProductCardProps {
  data: Product
}

export const ProductCard: FC<ProductCardProps> = ({ data }: ProductCardProps): ReactElement => {
  const {category, title, price, newPrice, isNew, isSale, isPopular } = data;

  const discountJSX = newPrice && (
    <div className={cx(styles.label, styles.discountLabel)}>
      <span>-{Math.round( 100 - (newPrice / (price / 100)))}%</span>
    </div>
  )

  const saleJSX = (
    <div className={cx(styles.label, styles.saleLabel)}>
      <span>акция</span>
    </div>
  )

  const newLabelJSX = (
    <div className={cx(styles.label, styles.newLabel)}>
      <span>new</span>
    </div>
  )

  const popularJSX = (
    <div className={cx(styles.label, styles.popularLabel)}>
      <span>Топ продаж</span>
    </div>
  )

  return (
    <div className={styles.wrapper}>
      { isNew && newLabelJSX }
      { newPrice && discountJSX }
      { isSale && saleJSX }
      { isPopular && popularJSX }
      <Link href="/catalog">
        <a>
          <img className={styles.image} src="/images/card-image.jpg" alt="Блуза белая"/>
        </a>
      </Link>
      <div className={styles.row}>
        <span className={styles.category}>{category.title}</span>
        <i className={styles.like}/>
      </div>
      <Link href="/catalog">
        <a>
          <h3 className={styles.title}>{title}</h3>
        </a>
      </Link>
      {newPrice ? (
        <div className={styles.price}>
          <span className={styles.oldPrice}>${price}</span>
          <span className={styles.newPrice}>${newPrice}</span>
        </div>
      ) : (
        <div className={styles.price}>${price}</div>
      )}
      <Colors/>
    </div>
  )
}
