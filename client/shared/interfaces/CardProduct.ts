export interface CartProduct {
  id: number;
  title: string;
  image: string;
  color: string;
  size: string;
  count: number;
  price: number;
}
