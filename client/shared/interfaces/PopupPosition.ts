export enum PopupPosition {
  Center = 'center',
  Right = 'right',
  FullScreen = 'fullscreen'
}
