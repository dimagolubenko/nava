// Interfaces
import {Category} from "@/shared/interfaces/Category";
import {Tag} from "@/shared/interfaces/Tag";
import {Image} from "@/shared/interfaces/Image";
import {Size} from "@/shared/interfaces/Size";
import {Color} from "@/shared/interfaces/Color";

export interface Product {
  id: number;
  title: string;
  price: number;
  newPrice?: number;
  isNew?: boolean;
  isPopular?: boolean;
  isSale?: boolean;
  description: string;
  inStock: number;
  sku: string;
  sizes: Size[];
  colors: Color[];
  category: Category;
  tags: Tag[];
  images: Image[];
}
