export interface Size {
  id: number;
  title: string;
  inStock?: number;
}
