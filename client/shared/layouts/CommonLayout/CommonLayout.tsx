// Core
import { ReactNode } from "react"
import styles from './CommonLayout.module.scss'

// Components
import { CommonHeader } from "@/shared/components/CommonHeader"
import { Footer } from '@/shared/components/Footer'

type Props = {
    children: ReactNode
}

export const CommonLayout = ({children}: Props) => {
    return (
        <div className={styles.layout}>
            <CommonHeader/>
            <main className={styles.main}>{children}</main>
            <Footer/>
        </div>
    )
}
